const crypto = require("node:crypto");
const fs = require("node:fs/promises");
const https = require("node:https");

const axios = require("axios");
const bsv = require("bsv");

const sha256 = (data) => {
  return crypto.createHash("sha256").update(data).digest();
};

const sha256d = (data) => {
  return sha256(sha256(data));
};

const stobitid = (s) => {
  return Buffer.from(s, "hex").reverse();
};

const httpsAgent = new https.Agent({ keepAlive: true });
const whatsonchain = axios.create({
  httpsAgent,
  baseURL: "https://api.whatsonchain.com/v1/bsv/test",
});

const headers = new Map();
const getBlockHeader = async (blkid) => {
  if (headers.has(blkid)) {
    return headers.get(blkid);
  }

  const response = await whatsonchain.get(`/block/${blkid}/header`);
  headers.set(blkid, response.data);
  return response.data;
};

const verify = async (datapoint, measurement) => {
  const script = new bsv.Script();

  script.writeOpCode(bsv.OpCode.OP_FALSE);
  script.writeOpCode(bsv.OpCode.OP_RETURN);
  script.writeBuffer(Buffer.from(datapoint.timestamp));
  script.writeBuffer(Buffer.from(datapoint.sensor));
  script.writeBuffer(Buffer.from(measurement));
  script.writeBuffer(Buffer.from(String(datapoint[measurement])));

  if (script.toHex() !== datapoint.script) {
    console.log("script does not match");
    return false;
  }

  if (!datapoint.transaction.includes(script.toHex())) {
    console.log("transaction does not contain script");
    return false;
  }

  const expectedTxid = stobitid(datapoint.txid);
  const actualTxid = sha256d(Buffer.from(datapoint.transaction, "hex"));

  if (Buffer.compare(expectedTxid, actualTxid) !== 0) {
    console.log("txid does not match");
    return false;
  }

  const header = await getBlockHeader(datapoint.proof.target);
  if (!header) {
    console.log("header not found");
    return false;
  }

  const txid = actualTxid;
  const merkleRoot = stobitid(header.merkleroot);

  let i = datapoint.proof.index;
  let prev = txid;
  for (const node of datapoint.proof.nodes) {
    const parts = [prev, stobitid(node)];

    if (i % 2 === 1) {
      parts.reverse();
    }

    prev = sha256d(Buffer.concat(parts));

    i = Math.trunc(i / 2);
  }

  if (Buffer.compare(prev, merkleRoot) !== 0) {
    console.log("merkle proof does not match merkleroot");
    return false;
  }

  return true;
};

const datapoints = require("./results.json");

const MEASUREMENT = process.env.MEASUREMENT;
const main = async () => {
  for (const datapoint of datapoints) {
    datapoint.valid = await verify(datapoint, MEASUREMENT);
    console.log(datapoint);
  }

  await fs.writeFile("./results.json", JSON.stringify(datapoints, null, 2));
};

main();
