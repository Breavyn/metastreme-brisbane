const crypto = require("node:crypto");
const fs = require("node:fs/promises");
const https = require("node:https");

const axios = require("axios");
const bsv = require("bsv");

const httpsAgent = new https.Agent({ keepAlive: true });
const metastreme = axios.create({
  httpsAgent,
  baseURL: "https://portal.metastreme.com/api",
  headers: { Authorization: "ApiKey 1b76b589-a718-4c3e-943f-8a635ffe620f" },
});

const datapoints = require("./results.json");

const main = async () => {
  for (const datapoint of datapoints) {
    // skip if we have the needed information for this point already
    if (datapoint.proof && datapoint.txid && datapoint.transaction) {
      continue;
    }

    try {
      // check request status
      const status = await metastreme.get(
        `/request/${datapoint.requestid}/status`
      );

      if (status.data.txid) {
        // transaction has been created, let's get a copy
        const transaction = await metastreme.get(
          `/transaction/${status.data.txid}`,
          { responseType: "arraybuffer" }
        );

        datapoint.txid = status.data.txid;
        datapoint.transaction = transaction.data.toString("hex");
      }

      if (status.data.proof) {
        datapoint.proof = status.data.proof;
      }

      console.log(datapoint);
    } catch (err) {
      console.log(err);
    }
  }

  await fs.writeFile("./results.json", JSON.stringify(datapoints, null, 2));
};

main();
