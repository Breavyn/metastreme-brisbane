`results.json` contains the results of completing the exercise using *pressure* as the measurement.

Results where created by running each script.
```sh
# Create scripts and request ids
MEASUREMENT="pressure" node step1.js

# Send requests to MetaStreme
# This command can be rerun as necessary if any requests fail
node step2.js

# Retrieve transaction and proof
# This command can be rerun as necessary until all needed data is
# available (essentially after a block has been mined)
node step3.js

# Verify
MEASUREMENT="pressure" node step4.js
```
