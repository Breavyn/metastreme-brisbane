const crypto = require("node:crypto");
const fs = require("node:fs/promises");
const https = require("node:https");

const axios = require("axios");
const bsv = require("bsv");

const httpsAgent = new https.Agent({ keepAlive: true });
const metastreme = axios.create({
  httpsAgent,
  baseURL: "https://portal.metastreme.com/api",
  headers: { Authorization: "ApiKey 1b76b589-a718-4c3e-943f-8a635ffe620f" },
});

const datapoints = require("./results.json");

const main = async () => {
  for (let i = 0; i < datapoints.length; i++) {
    const datapoint = datapoints[i];

    // skip datapoints that have already been submitted
    if (datapoint.sent) {
      continue;
    }

    try {
      // create request
      const request = {
        v: "1",
        id: datapoint.requestid,
        script: datapoint.script,
      };

      // send request
      await metastreme.post("/request", request);
      datapoint.sent = true;
      console.log(i, "sent", datapoint.requestid);
    } catch (err) {
      datapoint.sent = false;
      console.log(err);
    }
  }

  await fs.writeFile("./results.json", JSON.stringify(datapoints, null, 2));
};

main();
