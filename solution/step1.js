const crypto = require("node:crypto");
const fs = require("node:fs/promises");
const https = require("node:https");

const axios = require("axios");
const bsv = require("bsv");

const httpsAgent = new https.Agent({ keepAlive: true });
const metastreme = axios.create({
  httpsAgent,
  baseURL: "https://portal.metastreme.com/api",
  headers: { Authorization: "ApiKey 1b76b589-a718-4c3e-943f-8a635ffe620f" },
});

const datapoints = require("./datapoints.json");

const MEASUREMENT = process.env.MEASUREMENT;

const main = async () => {
  const results = datapoints.map((x) => {
    const script = new bsv.Script();

    script.writeOpCode(bsv.OpCode.OP_FALSE);
    script.writeOpCode(bsv.OpCode.OP_RETURN);
    script.writeBuffer(Buffer.from(x.timestamp));
    script.writeBuffer(Buffer.from(x.sensor));
    script.writeBuffer(Buffer.from(MEASUREMENT));
    script.writeBuffer(Buffer.from(String(x[MEASUREMENT])));

    return {
      timestamp: x.timestamp,
      sensor: x.sensor,
      [MEASUREMENT]: x[MEASUREMENT],
      script: script.toHex(),
      requestid: crypto.randomUUID(),
      txid: null,
      transaction: null,
      proof: null,
    };
  });

  await fs.writeFile("./results.json", JSON.stringify(results, null, 2));
};

main();
