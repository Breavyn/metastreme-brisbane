# Internal Notes

This exercise demonstrates how we can use the BSV blockchain to create a proof
that a data point existed at a certain time.

This will require some knowledge before/during the exercise.

- What a complete proof is

  - data point is in an output script.
  - output script is in a transaction.
  - transaction has a transaction id.
  - transaction id has a merkle proof.
  - merkle root is in a block header.
  - we trust the block header, i.e. it is an ancestor of what we consider to be
    the current chain tip.

- What a transaction output script is, especially regarding op return

- What a transaction id is

  - and how the string representation is reversed
  - also that string representation of block id, merkle root, merkle tree
    nodes, etc. are often reversed.

- what a merkle proof is and how the tsc json merkle proof format correlates

- that you should have a trusted source of block headers (in the exercise we
  use WhatsOnChain as a trusted source)
  - full/lite node
  - trusted 3rd party

# Issues

The merkle proof format spec page has a link to an example merkle proof
validator written in javascript.

https://tsc.bitcoinassociation.net/standards/merkle-proof-standardised-format/

# Create Dataset

The dataset was created by doing the following.

Brisbane Channel - `1KYmJu71uCEyETGE2CCJrL8uAGz6w3kVy8`

```
% curl -s 'https://weathersv.app/api/channel/1KYmJu71uCEyETGE2CCJrL8uAGz6w3kVy8/weather?start=2022-05-25T13:00:00.000Z&end=2022-07-14T00:00:00.000Z' > response.json

% cat response.json | jq 'length'
1000

% cat response.json | jq '[.[] | {timestamp: .datetime, sensor: "1KYmJu71uCEyETGE2CCJrL8uAGz6w3kVy8", temperature: .conditions.t, humidity: .conditions.h, pressure: .conditions.p, wind: .conditions.ws}]' > datapoints.json
```
